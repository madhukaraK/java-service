package com.madlabs.servicebase.controller.common;


public class Request {

    private Integer pageStart;
    private Integer pageEnd;

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(Integer pageEnd) {
        this.pageEnd = pageEnd;
    }

    @Override
    public String toString() {
        return new StringBuilder("Request {")
                .append("pageStart=").append(pageStart).append(", ")
                .append("pageEnd=").append(pageEnd)
                .append('}').toString();
    }
}
