package com.madlabs.servicebase.controller.common;

public class ErrorResponse extends Response {

    private Integer status;
    private Integer code;
    private String message;
    private String details;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return new StringBuilder("ErrorResponse {")
                .append("status=").append(status).append(", ")
                .append("code=").append(code).append(", ")
                .append("message='").append(message).append("'").append(", ")
                .append("details='").append(details).append("'")
                .append('}').toString();
    }
}
