package com.madlabs.servicebase.controller.common;

import com.madlabs.servicebase.modules.common.ServiceContext;

public abstract class GenericController {

    protected ServiceContext getServiceContext(final Request _request, final boolean _isPageable) {
        ServiceContext serviceContext = new ServiceContext();

        if (_isPageable) {
            Integer pageStart = 0;
            Integer pageEnd = null;
            Integer pageSize = null;

            if (_request != null) {
                Integer requestPageStart = _request.getPageStart();
                Integer requestPageEnd = _request.getPageEnd();
                if (requestPageStart != null && requestPageStart >= 0) {
                    pageStart = requestPageStart;
                }
                if (requestPageEnd != null && requestPageEnd >= 0) {
                    pageEnd = requestPageEnd;
                }
            }
            if (pageEnd != null) {
                if (pageEnd < pageStart) {
                    pageEnd = pageStart;
                }
                pageSize = pageEnd - pageStart + 1;
            }

            serviceContext.setPageable(_isPageable);
            serviceContext.setPageStart(pageStart);
            serviceContext.setPageEnd(pageEnd);
            serviceContext.setPageSize(pageSize);
        }

        return serviceContext;
    }

    protected ServiceContext getServiceContext(final boolean _isPageable) {
        return getServiceContext(null, _isPageable);
    }
}
