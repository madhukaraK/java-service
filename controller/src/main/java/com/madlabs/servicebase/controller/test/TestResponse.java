package com.madlabs.servicebase.controller.test;

import com.madlabs.servicebase.controller.common.Response;

/**
 * Created by Madhukara on 1/4/18.
 */
public class TestResponse extends Response{

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
