package com.madlabs.servicebase.controller.common;

import com.madlabs.servicebase.modules.common.UnauthorizedRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DefaultExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(final Exception _exception) {
        log.error("Unable to process request", _exception);

        ErrorResponse response = new ErrorResponse();
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setMessage("Unable to process request");
        response.setDetails(_exception.getMessage());

        return response;
    }

    @ExceptionHandler(UnauthorizedRequestException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public Response handleUnauthorizedRequestException(final UnauthorizedRequestException _exception) {
        log.error("Unauthorized request", _exception);

        ErrorResponse response = new ErrorResponse();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setCode(HttpStatus.UNAUTHORIZED.value());
        response.setMessage("Unauthorized request");
        response.setDetails(_exception.getMessage());

        return response;
    }
}
