package com.madlabs.servicebase.controller.test;

import com.madlabs.servicebase.controller.common.GenericController;
import com.madlabs.servicebase.controller.common.Response;
import com.madlabs.servicebase.modules.common.RequestStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * Created by Madhukara on 1/4/18.
 */
@Controller
@RequestMapping(value = "/", produces = "application/json")
public class TestController extends GenericController {

    @RequestMapping(value = "test", method = RequestMethod.GET)
    @ResponseBody
    public Response initializeRegistration() {

        TestResponse response = new TestResponse();

        response.setStatus(RequestStatus.SUCCESS.getStatus());
        response.setMessage("Hello...! Welcome to Service Base.");


        return response;
    }
}
