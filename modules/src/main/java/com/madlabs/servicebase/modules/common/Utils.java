package com.madlabs.servicebase.modules.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Utils {

    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static Date getCurrentUtcDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        SimpleDateFormat dateFormatUtc = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        dateFormatUtc.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = new Date();
        Date utc = null;
        try {
            utc = dateFormat.parse(dateFormatUtc.format(date));
        } catch (ParseException exception) {
            log.warn("Unable to obtain UTC date using local date, defaulting to local date", exception);
            utc = date;
        }
        return utc;
    }

    public static Date getCurrentIstDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        SimpleDateFormat dateFormatIst = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        dateFormatIst.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = new Date();
        Date ist = null;
        try {
            ist = dateFormat.parse(dateFormatIst.format(date));
        } catch (ParseException exception) {
            log.warn("Unable to obtain IST date using local date, defaulting to local date", exception);
            ist = date;
        }
        return ist;
    }

    private Utils() {
    }
}
