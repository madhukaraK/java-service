package com.madlabs.servicebase.modules.common;

/**
 * Enumeration of request statuses.
 */
public enum RequestStatus {

    SUCCESS("success"),
    FAILURE("failure");

    private String status;

    private RequestStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static RequestStatus getByStatus(String status) {
        for (RequestStatus requestStatus : values()) {
            if (requestStatus.getStatus().equals(status)) {
                return requestStatus;
            }
        }
        throw new AssertionError("Request status not found for given status [status: " + status + "]");
    }
}
