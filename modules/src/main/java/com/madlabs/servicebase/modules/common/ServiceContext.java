package com.madlabs.servicebase.modules.common;

import java.util.Locale;

public class ServiceContext {

    // pagination
    private Integer pageStart;
    private Integer pageEnd;
    private Integer pageSize;
    private boolean isPageable;

    // i18n
    private Locale locale = Locale.ENGLISH;

    private ServiceContext copy;

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageEnd() {
        return pageEnd;
    }

    public void setPageEnd(Integer pageEnd) {
        this.pageEnd = pageEnd;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isPageable() {
        return isPageable;
    }

    public void setPageable(boolean isPageable) {
        this.isPageable = isPageable;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
        if (copy != null) {
            copy.setLocale(this.locale);
        }
    }

    public ServiceContext getCopy() {
        if (copy == null) {
            copy = new ServiceContext();
            copy.setLocale(this.locale);
        }
        return copy;
    }
}
