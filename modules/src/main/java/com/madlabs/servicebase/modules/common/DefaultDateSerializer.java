package com.madlabs.servicebase.modules.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DefaultDateSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(final Date _value, final JsonGenerator _generator, final SerializerProvider _serializerProvider)
            throws IOException, JsonProcessingException {

        _generator.writeString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(_value));
    }
}
