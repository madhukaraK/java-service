package com.madlabs.servicebase.modules.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;

public abstract class GenericService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Environment environment;

    protected MessageSource getMessageSource() {
        return messageSource;
    }

    protected Environment getEnvironment() {
        return environment;
    }

    protected String getValidationFailureMessage(final String _code, final ServiceContext _serviceContext) {
        String message = messageSource.getMessage(_code, null, "Validation failure", _serviceContext.getLocale());

        return message;
    }

    protected String getGeneralValidationFailureMessage(final String _reason, final ServiceContext _serviceContext) {
        String message = messageSource.getMessage("common.validation.failure.general",
                new Object[]{_reason}, _serviceContext.getLocale());

        return message;
    }

    protected String getMandatoryValidationFailureMessage(final String _field, final ServiceContext _serviceContext) {
        String message = messageSource.getMessage("common.validation.failure.mandatory",
                new Object[]{_field}, _serviceContext.getLocale());

        return message;
    }
}
