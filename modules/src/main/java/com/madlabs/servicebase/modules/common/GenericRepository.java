package com.madlabs.servicebase.modules.common;

import javax.persistence.Query;

public abstract class GenericRepository {

    protected void handlePagination(final Query _query, final ServiceContext _serviceContext) {
        if (_serviceContext != null && _serviceContext.isPageable()) {
            _query.setFirstResult(_serviceContext.getPageStart());
            if (_serviceContext.getPageSize() != null) {
                _query.setMaxResults(_serviceContext.getPageSize());
            }
        }
    }
}
