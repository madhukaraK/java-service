package com.madlabs.servicebase.modules.common;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractEntity {

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "created_datetime")
    private Date createdDatetime;

    @Column(name = "modified_by")
    private Long modifiedBy;

    @Column(name = "modified_datetime")
    private Date modifiedDatetime;

    @Version
    @Column(name = "version")
    private Integer version;

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return new StringBuilder("AbstractEntity {")
                .append("createdBy=").append(createdBy).append(", ")
                .append("createdDatetime=").append(createdDatetime).append(", ")
                .append("modifiedBy=").append(modifiedBy).append(", ")
                .append("modifiedDatetime=").append(modifiedDatetime).append(", ")
                .append("version=").append(version)
                .append('}').toString();
    }
}
